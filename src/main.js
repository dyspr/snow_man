var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var dimension = 8
var initSize = 0.8
var steps = 32
var frame = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  translate(windowWidth * 0.5, windowHeight * 0.5)
  for (var i = 0; i < dimension; i++) {
    noStroke()
    push()
    for (var j = 0; j < pow(2, i); j++) {
      push()
      translate(0, (j - pow(2, i) * 0.5 + 0.5) * boardSize * initSize * (1 / pow(2, i)))
      fill((255 / 31) * (((i + 31) * abs(sin(frame + j * (1 / dimension))) + (j + 31) * abs(sin(frame + i * (1 / dimension)))) % 32))
      ellipse(0, 0, boardSize * initSize * (1 / pow(2, i)))
      pop()
    }
    pop()
  }
  frame += deltaTime * 0.001
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
